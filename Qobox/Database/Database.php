<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

namespace Qobox\Database;

class Database {
	
	public $Connection;
	private $Statement;
	protected $PARAM_STR = \PDO::PARAM_STR;
    protected $Reflection;
    
	public function __construct()
	{
		$DatabaseConnect = include "Config/Database.php";
		
		try {
			$this->Connection = new \PDO($DatabaseConnect["DBEngine"].":host=".$DatabaseConnect["DBHost"].";dbname=".$DatabaseConnect["DBName"].";charset=utf8", $DatabaseConnect["DBUser"], $DatabaseConnect["DBPass"]);
			$this->Connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false); 
			$this->Connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch(\PDOException $e) {
			echo "Connection to the database failed: ".$e->getMessage();
		}
	}

	public function __call($name, $parameterList)
	{
		$callableArray = array("beginTransaction", "commit", "errorCode", "errorInfo", "execute", "getAttribute", "inTransaction", "lastInsertId", "quote", "rollBack");

		if(in_array($name, $callableArray)) {
            return $this->Reflection->invoke("PDO", $name, $parameterList, $this->Statement);
		}
		return false;
	}

	/**
	 * @param String $tableName
	 * @return bool
	 */
	public function __get(String $tableName): bool
	{
		if(!$this->$tableName instanceof \Qobox\Database\Table)
        {
			$this->$tableName = new \Qobox\Database\Table($tableName, $this->Connection, $this->Reflection);
			return true;
		}
        return false;
	}

	/**
	 * @return string
	 */
	public function lastID(): string
	{
		return $this->Connection->lastInsertId();
	}

	/**
	 * @param String $statement
	 * @return bool
	 */
	public function prepare(String $statement): bool
	{
		try
        {
			$this->Statement = $this->Connection->prepare($statement);
			return $this->Statement;
		}
        catch(\PDOException $e)
        {
			print_r($e->getTrace());
			echo "Preparing an SQL statement failed: ".$e->getMessage();
			return false;
		}
	}

	/**
	 * @param int $attribute
	 * @param $value
	 * @return bool
	 */
	public function setAttribute(int $attribute, $value): bool
	{
		return $this->Connection->setAttribute($attribute, $value);
	}
}