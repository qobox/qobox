<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license
 */

namespace Qobox\Database;

class Table
{
	private $Connection;
	private $Statement;
	private $Table;
    protected $Reflection;

	public function __construct($tableName, $connection, $reflection)
	{
		$this->Table = $tableName;
		$this->Connection = $connection;
        $this->Reflection = $reflection;
	}

    /**
     * @param String $name
     * @param array $parameterList
     * @return mixed
     */
    public function __call(String $name, Array $parameterList)
	{
		$callableArray = array("bindParam", "bindValue", "closeCursor", "columnCount", "debug", "errorCode", "errorInfo", "execute", "fetch", "fetchColumn", "fetchObject", "nextRowset", "rowCount");

		if(in_array($name, $callableArray))
        {
            return $this->Reflection->invoke("PDOStatement", $name, $parameterList, $this->Statement);
		}
		return false;
	}

	/**
	 * @param array $conditionArray
	 * @param String $joinParams
	 * @return bool
	 */
	public function delete(Array $conditionArray, String $joinParams = "AND"): bool
	{
		$implodeArray = array();
		$bindParameters = array();

		foreach($conditionArray AS $key => $value)
        {
			$implodeArray[] = $key.' = :'.$key;
			$bindParameters[] = array(':'.$key, $value, \PDO::PARAM_STR);
		}

		$statement = "DELETE FROM ".$this->Table." WHERE ".implode(' '.$joinParams.' ', $implodeArray);
		return $this->prepareExecute($statement, $bindParameters);
	}

	/**
	 * @param int $fetchStyle
	 * @param string $fetchArgument
	 * @param array $constructorArguments
	 * @return mixed
	 */
	public function fetchAll(int $fetchStyle = \PDO::FETCH_OBJ, $fetchArgument = '', Array $constructorArguments = array())
	{
		if($fetchArgument != '' && empty($constructorArguments))
        {
			return $this->Statement->fetchAll($fetchStyle, $fetchArgument);
		}
        else if($fetchArgument != '' && !empty($constructorArguments))
        {
			return $this->Statement->fetchAll($fetchStyle, $fetchArgument, $constructorArguments);
		}
        else
        {
			return $this->Statement->fetchAll($fetchStyle);
		}
        return false;
	}

	/**
	 * @param array $columnArray
	 * @return bool
	 */
	public function insert(Array $columnArray): bool
	{
		$statement = "INSERT INTO ".$this->Table;
		$keyArray = array_keys($columnArray);
		$valueArray = array();
		$parameterArray = array();

		foreach($keyArray as $key)
        {
			$valueArray[$key] = ':'.$key;
			$parameterArray[] = array(':'.$key, $columnArray[$key], \PDO::PARAM_STR);
		}

		$statement .= ' ('.implode(',',$keyArray).') VALUES ('.implode(',',$valueArray).')';
		$prepare = $this->prepareExecute($statement, $parameterArray);
		$this->closeCursor();
		return $prepare;
	}

	/**
	 * @param String $statement
	 * @return bool|PDOStatement object
	 */
	public function prepare(String $statement)
	{
		try
        {
			$this->Statement = $this->Connection->prepare($statement);
			return $this->Statement;
		}
        catch(\PDOException $e)
        {
			print_r($e->getTrace());
			echo "Preparing an SQL statement failed: ".$e->getMessage();
		}
        return false;
	}

	/**
	 * @param String $statement
	 * @param array $bindParameters
	 * @return bool
	 */
	private function prepareExecute(String $statement, Array $bindParameters = array()): bool
	{
		try
        {
			$this->prepare($statement);

			if(!empty($bindParameters))
            {
				foreach($bindParameters as $parameter)
                {
					$this->bindValue($parameter[0],$parameter[1],$parameter[2]);
				}
			}
			return $this->execute();
		}
        catch(\PDOException $e)
        {
			print_r($e->getTrace());
			echo "Executing an SQL statement failed: ".$e->getMessage();
		}
        return false;
	}

	/**
	 * @param String $columns
	 * @param String $condition
	 * @return bool
	 */
	private function select(String $columns = "", String $condition = ""): bool
	{
//        print_r($columns);
//        exit;
		if(empty($columns))
        {
			$columns = "*";
		}

		$statement = "SELECT ".$columns." FROM ".$this->Table;

		if(!empty($condition))
        {
			$statement .= " ".$condition;
		}
		return $this->prepareExecute($statement);
	}

	/**
	 * @param String $columns
	 * @param String $condition
	 * @param int $fetchStyle
	 * @param string $fetchArgument
	 * @param array $constructorArguments
	 * @return mixed
	 */
	public function selectAll(String $columns = "", String $condition = "", int $fetchStyle = \PDO::FETCH_OBJ, $fetchArgument = '', Array $constructorArguments = array())
	{
		$this->select($columns, $condition);
		return $this->fetchAll($fetchStyle, $fetchArgument, $constructorArguments);
	}

	/**
	 * @param String $columns
	 * @param String $condition
	 * @param int $columnNumber
	 * @return mixed
	 */
	public function selectColumn(String $columns = "", String $condition = "", int $columnNumber = 0)
	{
		$this->select($columns, $condition);
		return $this->fetchColumn($columnNumber);
	}

	/**
	 * @param String $columns
	 * @param String $condition
	 * @param String $className
	 * @param array $constructorArguments
	 * @return mixed
	 */
	public function selectObject(String $columns = "", String $condition = "", String $className = "\\stdClass", Array $constructorArguments = array())
	{
		$this->select($columns, $condition);
		return $this->fetchObject($className, $constructorArguments);
	}

	/**
	 * @param String $columns
	 * @param String $condition
	 * @param int $fetchStyle
	 * @param int $cursorOrientation
	 * @param int $cursorOffset
	 * @return mixed
	 */
	public function selectRow(String $columns = "", String $condition = "", int $fetchStyle = \PDO::FETCH_OBJ, int $cursorOrientation = \PDO::FETCH_ORI_NEXT, int $cursorOffset = 0)
	{
		$this->select($columns, $condition);
		return $this->fetch($fetchStyle, $cursorOrientation, $cursorOffset);
	}

	/**
	 * @param array $columnArray
	 * @param String $condition
	 * @return bool
	 */
	public function update(Array $columnArray, String $condition): bool
	{
		$statement = 'UPDATE '.$this->Table.' SET ';

		$keyArray = array_keys($columnArray);
		$valueArray = array();
		$bindParameters = array();
		$implodeArray = array();

		foreach($keyArray as $key)
        {
			$valueArray[$key] = ':'.$key;
			$implodeArray[] = $key.' = :'.$key;
			$bindParameters[] = array(':'.$key, $columnArray[$key], \PDO::PARAM_STR);
		}

		$statement .= implode(', ',$implodeArray).' '.$condition;
		return $this->prepareExecute($statement, $bindParameters);
	}
}