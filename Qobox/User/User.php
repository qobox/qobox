<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

namespace Qobox\User;

class User
{

    protected $Database;
    protected $Strings;
    protected $Settings;

    /**
     * @param String $permission
     * @return bool
     */
    public function __get(String $permission): bool
    {
        // permissions may looks like this:shop_Buy
        // meaning "grab Buy permission from table shopPermission"
        // that's why we need to explode them

        $permissionArray = explode("_", $permission);
        $table = "";

        if (count($permissionArray) == 2)
        {
            $table = $permissionArray[0];
            $columns = $permissionArray[1];
        }
        else
        {
            $columns = $permissionArray[0];
        }

        $tableName = $table . 'Permission';
        @$this->Database->$tableName;
        $result = $this->Database->$tableName->selectRow($columns, "LEFT JOIN User AS u ON u.userGroup = groupID WHERE userSID = '" . session_id() . "' AND userIp = '" . $_SERVER['REMOTE_ADDR'] . "' LIMIT 1");
        return $result->$columns;
    }

    /**
     * @param String $permission
     * @param String $value
     * @return bool
     */
    public function __set(string $permission, string $value): bool
    {
        $sessionID = session_id();

        $permissionArray = explode("_", $permission);
        $table = "";

        if (count($permissionArray) == 2)
        {
            $table = $permissionArray[0];
            $columns = $permissionArray[1];
        }
        else
        {
            $columns = $permissionArray[0];
        }

        $tableName = $table . 'Permission';
        @$this->Database->$tableName;
        return $this->Database->$tableName->update(array($columns => $value), "WHERE userSID = '" . $sessionID . "' AND userIp = '" . $_SERVER['REMOTE_ADDR'] . "'");
    }

    public function login(string $username, string $password): bool
    {
        if ($username == "" || $password == "")
        {
            return false;
        }

        $password = $this->Strings->hashString($password);

        @$this->Database->User;
        $userID = $this->Database->User->selectRow("userID", "WHERE userName = '" . $username . "' AND userPassword = '" . $password . "'")->userID;
        if ($userID > 0)
        {
            @$this->Database->User;
            return $this->Database->User->update(array("userSID" => session_id(), "userIp" => $_SERVER['REMOTE_ADDR']), "WHERE userID = '" . $userID . "'");
            $this->regenerateID();
        }
        return false;
    }

    public function logout(): bool
    {
        $_SESSION = array();
        session_destroy();
        return true;
    }

    public function regenerateID(): bool
    {
        // eliminate session id prediction, generate pseudorandom LONG session id
        session_id($this->Strings->randomString(128, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"));
        @$this->Database->User;
        $userID = $this->Database->User->selectRow("userID", "WHERE userSID = '" . session_id() . "' AND userIp = '" . $_SERVER['REMOTE_ADDR'] . "'")->userID;
        return $this->Database->User->update(array("userSID" => session_id()), "WHERE userID = '" . $userID . "'");
    }

    public function register(string $username, string $password, string $email, bool $registered = true, int $group = 0): bool
    {
        session_regenerate_id();
        $password = $this->Strings->hashString($password);

        $insertArray = array(
            "userGroup" => $group,
            "userName" => $username,
            "userPassword" => $password,
            "userMail" => $email,
            "userSID" => session_id(),
            "userIp" => $_SERVER["REMOTE_ADDR"],
            "userRegged" => $registered
        );

        @$this->Database->User;
        return $this->Database->User->insert($insertArray);
    }
}