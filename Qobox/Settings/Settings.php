<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

namespace Qobox\Settings;

class Settings
{
	
	protected $Database;

	/**
	 * @param String $settingName
	 * @return string
	 */
	public function __get(String $settingName): string
	{
		@$this->Database->Settings;
		$result = $this->Database->Settings->selectRow("value", "WHERE setting = '".$settingName."'");
		return $result->value;
	}

	/**
	 * @param String $settingName
	 * @param $settingValue
	 * @return bool
	 */
	public function __set(String $settingName, String $settingValue): bool
	{
		@$this->Database->Settings;
		return $this->Database->Settings->update(array("value" => $settingValue), "WHERE setting = '".$settingName."' AND unlocked = 1");
	}
}