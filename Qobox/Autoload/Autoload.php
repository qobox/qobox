<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

spl_autoload_register(function($Class) {
	$include = str_replace("\\", DIRECTORY_SEPARATOR, $Class).".php";
	if(file_exists($include)) {
		require_once $include;
	}
});

spl_autoload_register(function($Class) {
	$include = str_replace("\\", DIRECTORY_SEPARATOR, $Class).DIRECTORY_SEPARATOR."Main.php";
	if(file_exists($include)) {
		require_once $include;
	}
});
?>
