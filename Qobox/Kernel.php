<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

namespace Qobox;

class Kernel
{
	protected $request;
	protected $module;

	/**
	 * Kernel constructor.
	 */
	public function __construct()
	{
		$this->request = $this->getRequest();
		$requestedModuleName = $this->getModule();
		$moduleInstance = $this->getModuleInstance($requestedModuleName);
		$this->setModuleProperties($moduleInstance);
		$moduleMethod = $this->getModuleMethod();
		$argumentArray = $this->setMethodArguments($moduleMethod);
		$moduleMethod->invokeArgs($moduleInstance, $argumentArray);
	}

	/**
	 * Grab the Fully Qualified Module Name, including namespace
	 *
	 * @return string $FullyQualifiedModuleName
	 */
	private function getModule(): string
	{
		$moduleList = require "Config/Modules.php";
		$namespaceArray = $this->getModuleNamespace($moduleList);
		return implode("\\", $namespaceArray);
	}

	/**
	 * Get the requested module instance
	 *
	 * @param $requestedModuleName
	 * @return object
	 */
	private function getModuleInstance($requestedModuleName)
	{
		try {
			$this->module = new \ReflectionClass("\\Output\\".$requestedModuleName);
		} catch (\ReflectionException $e) {
			try {
				$this->module = new \ReflectionClass("\\Module\\" . $requestedModuleName);
			} catch (\ReflectionException $e) {
				$e->callingClass = $requestedModuleName;
				echo '<pre>';
				print_r($e);
				exit;
			}
		}

		if ($this->module->isInstantiable() === TRUE) {
			return $this->module->newInstance();
		} else
		{
			// todo: proper user-friendly error handler
			echo 'Module is not instantiable';
			exit;
		}
	}

	/**
	 * Get the requested method of the requested module
	 *
	 * @return mixed
	 */
	private function getModuleMethod()
	{
		$moduleMethods = $this->module->getMethods(\ReflectionMethod::IS_PUBLIC);

		foreach ($moduleMethods as $methodKey => $method) {
			if ($method->name == "__construct") {
				unset($moduleMethods[$methodKey]);
			}
		}
		$moduleMethods = array_values($moduleMethods);

		if (!empty($this->request) && $this->module->hasMethod(array_keys($this->request)[0])) {
			// the method exists, load it
			$moduleMethod = $this->module->getMethod(array_shift($this->request));
		} else {
			$moduleMethod = $moduleMethods[0];
		}

		return $moduleMethod;
	}

	/**
	 * Combines the full qualified namespace for requested module
	 * Recursive function
	 *
	 * @param array $moduleList
	 * @param array $namespaceArray
	 * @return array
	 */
	private function getModuleNamespace(Array $moduleList, Array $namespaceArray = array()): array
	{
		if (!empty($this->request) && (array_key_exists(array_keys($this->request)[0], $moduleList) || in_array(array_keys($this->request)[0], $moduleList))) {
			$loader = array_shift($this->request);
		} else if (is_array($moduleList[array_keys($moduleList)[0]])) {
			$loader = array_keys($moduleList)[0];
		} else {
			$loader = array_values($moduleList)[0];
		}

		$namespaceArray[] = $loader;

		if (array_key_exists($loader, $moduleList) && is_array($moduleList[$loader]) && !empty($moduleList[$loader])) {
			return $this->getModuleNamespace($moduleList[$loader], $namespaceArray);
		} else {
			return $namespaceArray;
		}
	}

	/**
	 * Get the module properties' instances
	 * Recursive function
	 *
	 * @param array $propertyValue
	 * @param String $moduleName
	 * @return \stdClass
	 */
	private function getModulePropertyInstance(Array $propertyValue, String $moduleName): \stdClass
	{
		$propertyClass = new \stdClass();
		foreach ($propertyValue as $moduleCaller => $moduleSubname) {
			if (is_array($moduleSubname) === TRUE) {
				$propertyClass->$moduleCaller = $this->getModulePropertyInstance($moduleSubname, $moduleName . "\\" . $moduleCaller);
			} else {
				$propertyClass->$moduleSubname = $this->getSubmodule($moduleName . "\\" . $moduleSubname);
			}
		}

		return $propertyClass;
	}

	/**
	 * Get the parsed _$REQUEST array
	 *
	 * @return array
	 */
	private function getRequest(): array
	{
		$request = array();
		foreach ($_REQUEST as $key => $value) {
			$key = htmlspecialchars($key, ENT_QUOTES | ENT_DISALLOWED | ENT_HTML5, "UTF-8");
			$value = htmlspecialchars($value, ENT_QUOTES | ENT_DISALLOWED | ENT_HTML5, "UTF-8");

			if (empty($value)) {
				$request[$key] = $key;
			} else {
				$request[$key] = $value;
			}
		}

		return $request;
	}

	/**
	 * Get submodule instance
	 *
	 * @param String $submoduleName
	 * @return object
	 */
	private function getSubmodule(String $submoduleName)
	{
		$namespacePrefixList = array("\\Qobox\\", "\\Module\\", "\\Output\\", "\\Qobox\\$submoduleName\\", "\\Module\\$submoduleName\\", "\\Output\\$submoduleName\\");
		foreach ($namespacePrefixList as $namespacePrefix) {
			$submoduleNamespace = $namespacePrefix . $submoduleName;
            
			try {
				$submodule = new \ReflectionClass($submoduleNamespace);
				break;
			} catch (\ReflectionException $e) {
			}
		}

		$submoduleInstance = $submodule->newInstance();
		$submoduleProperties = $submodule->getProperties(\ReflectionProperty::IS_PROTECTED);

		foreach ($submoduleProperties as $submoduleProperty) {
			$submoduleProperty->setAccessible(true);
			if (empty($submoduleProperty->getValue($submoduleInstance))) {
				$submoduleName = $submoduleProperty->name;
				$submoduleProperty->setValue($submoduleInstance, $this->getSubmodule($submoduleName));
			}
			$submoduleProperty->setAccessible(false);
		}
		return $submoduleInstance;
	}

	/**
	 * Set requested module arguments
	 *
	 * @param $moduleMethod
	 * @return array
	 */
	private function setMethodArguments($moduleMethod): array
	{
		$argumentArray = array();
		$moduleMethodArguments = $moduleMethod->getParameters();
		foreach ($moduleMethodArguments as $Parameter) {
			if (isset($this->request[$Parameter->name]) && !empty($this->request[$Parameter->name]) && $this->request[$Parameter->name] != $Parameter->name) {
				$argumentArray[$Parameter->name] = $this->request[$Parameter->name];
			} else {
				if ($Parameter->isDefaultValueAvailable() === TRUE) {
					$argumentArray[$Parameter->name] = $Parameter->getDefaultValue();
				} else {
					$argumentArray[$Parameter->name] = "";
				}
			}
		}
		return $argumentArray;
	}

	/**
	 * Set requested module properties
	 *
	 * @param $moduleInstance
	 */
	private function setModuleProperties($moduleInstance)
	{
		$moduleProperties = $this->module->getProperties(\ReflectionProperty::IS_PROTECTED);
		foreach ($moduleProperties as $property)
		{
			$moduleName = $property->name;
			$property->setAccessible(true);
			$propertyValue = $property->getValue($moduleInstance);

			if (is_array($propertyValue) === TRUE)
			{
				$propertyClass = $this->getModulePropertyInstance($propertyValue, $moduleName);
				$property->setValue($moduleInstance, $propertyClass);
			} else
			{
				$property->setValue($moduleInstance, $this->getSubmodule($moduleName . "\\" . $moduleName));
			}
		}
	}
}