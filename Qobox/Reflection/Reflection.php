<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

namespace Qobox\Reflection;

class Reflection
{
    public function invoke(String $reflectionClass, String $methodName, Array $parameterList, $object)
    {
        $instance = new \ReflectionClass("\\".$reflectionClass);

        try
        {
            $callableMethod = $instance->getMethod($methodName);
            $callableMethodParameterList = $callableMethod->getParameters();

            $parameterArray = array();
            $parameterListSize = count($parameterList);
            $callableMethodParameterSize = count($callableMethodParameterList);

            for($i=0; $i<$callableMethodParameterSize; $i++)
            {
                $methodParameter = $callableMethodParameterList[$i];

                if($i < $parameterListSize)
                {
                    $parameterArray[$methodParameter->name] = $parameterList[$i];
                }
                else
                {
                    if($methodParameter->isDefaultValueAvailable())
                    {
                        $parameterArray[$methodParameter->name] = $methodParameter->getDefaultValue();
                    }
                }
            }
            return $callableMethod->invokeArgs($object, $parameterArray);
        }
        catch(\ReflectionException $e)
        {
            echo "<pre>";
            debug_print_backtrace();
            print_r($e);
            echo "</pre>";
        }
    }
}