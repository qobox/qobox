<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

namespace Qobox\Number;

class Number
{

	/**
	 * @param String $string
	 * @return float
	 */
	public function currencyToFloat(String $string): float
	{
		$string = preg_replace("/[^0-9.,]/", "", $string);
		$string = str_replace(",", ".", $string);
		if(($strrpos = strrpos($string, ".", -1)) !== FALSE)
		{
			$split = str_split($string, $strrpos);
			$split[0] = str_replace(".", "", $split[0]);
			$string = $split[0].$split[1];
		}
		return (float) $string;
	}

	/**
	 * @param float $number
	 * @param String $decimalSeparator
	 * @param String $thousandSeparator
	 * @return string
	 */
	public function currencyFormat(float $number, String $decimalSeparator=",", String $thousandSeparator = "&nbsp;"): string
	{
		return number_format($number, 2, $decimalSeparator, $thousandSeparator);
	}
}