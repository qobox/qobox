<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

namespace Qobox\Strings;

class Strings
{

	protected $Settings;

	/**
	 * @param String $string
	 * @param int $saltRounds
	 * @param String $saltString
	 * @return String
	 */
	public function hashString(String $string, int $saltRounds = 5000, String $saltString =''): String
	{
		if($saltString == '' && $this->Settings->saltPasswordString == '')
        {
			$saltString = $this->randomString();
			$this->Settings->saltPasswordString = $saltString;
		}
        else if($saltString == '' && $this->Settings->saltPasswordString != '')
        {
			$saltString = $this->Settings->saltPasswordString;
		}
        else if($saltString != '' && $this->Settings->saltPasswordString == '')
        {
			$this->Settings->saltPasswordString = $saltString;
		}
		if(strlen($saltString) > 16)
		{
			$saltString = substr($saltString, 0, 16);
            $this->Settings->saltPasswordString = $saltString;
        }
		return str_replace('$6$rounds='.$saltRounds.'$'.$saltString.'$', '', crypt($string, '$6$rounds='.$saltRounds.'$'.$saltString.'$'));
	}

	/**
	 * @param int $length
	 * @param String $characters
	 * @return string
	 */
	public function randomString(int $length = 30, String $characters = "0123456789_-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"): string
	{
		$randomString = "";
		for ($i = 0; $i < $length; $i++)
        {
			$randomString .= $characters[rand(0, strlen($characters)-1)];
		}
		return $randomString;
	}
}