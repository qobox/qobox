<?php
/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license    
 */

error_reporting(-1);

session_start();

require_once "Qobox/Autoload/Autoload.php";

new Qobox\Kernel;