<?php
/**
 * Qobox Framework
 * Copyright (c) 2016. Flexbox (www.flexbox.cz or www.flexbox.it)
 * You may contact us also at support@qobox.net
 *
 * This file is part of Qobox.
 *
 * Qobox is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qobox is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qobox.  If not, see <http://www.gnu.org/licenses/>.
 */

$DatabaseConnect = array(
		"DBEngine" => "mysql",
		"DBName" => "qobox",
		"DBUser" => "root",
		"DBPass" => "",
		"DBHost" => "localhost"
	);

return $DatabaseConnect;