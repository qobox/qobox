/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license
 */

 /**
  *    USE THIS FILE FOR CLEAN INSTALLATION ONLY
  *    SEE UPDATE FOLDER IF YOU NEED TO UPDATE
  */


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qobox`
--

-- --------------------------------------------------------

--
-- Table structure for table `Permission`
--

CREATE TABLE IF NOT EXISTS `Permission` (
  `groupID` int(7) unsigned NOT NULL,
  `groupName` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `userAdmin` tinyint(1) NOT NULL COMMENT 'may do anything to users',
  `userView` tinyint(1) NOT NULL COMMENT 'may look at user list',
  `userAdd` tinyint(1) NOT NULL COMMENT 'may add new users',
  `userEdit` tinyint(1) NOT NULL COMMENT 'may edit user information',
  `userBlock` tinyint(1) NOT NULL COMMENT 'may block users'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Permission`
--

INSERT INTO `Permission` (`groupID`, `groupName`, `userAdmin`, `userView`, `userAdd`, `userEdit`, `userBlock`) VALUES
(0, 'User', 0, 0, 0, 0, 0),
(1, 'Admin', 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Settings`
--

CREATE TABLE IF NOT EXISTS `Settings` (
  `setting` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `unlocked` tinyint(1) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Settings`
--

INSERT INTO `Settings` (`setting`, `value`, `unlocked`) VALUES
('defaultMailSender', 'test@example.com', 1),
('saltPasswordString', 'Qobox', 0);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `userID` int(11) NOT NULL,
  `userGroup` int(1) NOT NULL COMMENT '1 for admin',
  `userName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userPassword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userMail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userSID` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'session_id',
  `userIp` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user IP address',
  `userRegged` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`userID`, `userGroup`, `userName`, `userPassword`, `userMail`, `userSID`, userIp, `userRegged`) VALUES
(1, 1, 'admin', '', '', '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Permission`
--
ALTER TABLE `Permission`
  ADD PRIMARY KEY (`groupID`);

--
-- Indexes for table `Settings`
--
ALTER TABLE `Settings`
  ADD UNIQUE KEY `setting` (`setting`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Permission`
--
ALTER TABLE `Permission`
  MODIFY `groupID` int(7) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
