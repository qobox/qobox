/**
 *     Qobox Framework 0.2 | (c) Flexbox 2014 - 2016 | qobox.net/license
 */

 /**
  *    USE THIS FILE TO UPGRADE FROM VERSION 0.1 TO VERSION 0.2
  */

ALTER TABLE User DROP userSecret;
ALTER TABLE User ADD userIp VARCHAR(45) NULL;
ALTER TABLE User MODIFY userSID VARCHAR(128) NOT NULL COMMENT 'session_id';